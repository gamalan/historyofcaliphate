/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(init);

function init() {
    /* ========== DRAWING THE PATH AND INITIATING THE PLUGIN ============= */

    $.fn.scrollPath("getPath")
    // Move to 'start' element
    .moveTo(400, 150, {
        name: "start"
    })
    .lineTo(1600, -150, {
        name:"makkahphase-1"
    })
    .lineTo(2800, -150, {
        name:"hijrah-1"
    })
    .lineTo(4000, -600, {
        name:"madinahphase-1"
    })
    .lineTo(5200, -600, {
        name:"abubakar-1"
    })
    .lineTo(6400, -600, {
        name:"umarbinkhattab-1"
    })
    .lineTo(7600, -600, {
        name:"utsmanbinaffan-1"
    })
    .lineTo(8800, -600, {
        name:"alibinabithalib-1"
    })
    .lineTo(10000, -450, {
        name:"baniumayyah-1"
    })
    .lineTo(11200, -450, {
        name:"baniabbasiyah-1"
    })
    .lineTo(12400, 150, {
        name:"kekosongan-1"
    })
    .lineTo(13600, -150, {
        name:"banimamalik-1"
    })
    .lineTo(14800, -450, {
        name:"baniutsmaniyah-1"
    })
    .lineTo(16000, -150, {
        name:"sekarang-1"
    })
    /*/ Line to 'description' element
    .lineTo(400, 800, {
        name: "description"
    })
    // Arc down and line to 'syntax'
    .arc(200, 1200, 400, -Math.PI/2, Math.PI/2, true)
    .lineTo(600, 1600, {
        callback: function() {
            highlight($(".settings"));
        },
        name: "syntax"
    })
    // Continue line to 'scrollbar'
    .lineTo(1750, 1600, {
        callback: function() {
            highlight($(".sp-scroll-handle"));
        },
        name: "scrollbar"
    })
    // Arc up while rotating
    .arc(1800, 1000, 600, Math.PI/2, 0, true, {
        rotate: Math.PI/2
    })
    // Line to 'rotations'
    .lineTo(2400, 750, {
        name: "rotations"
    })
    // Rotate in place
    .rotate(3*Math.PI/2, {
        name: "rotations-rotated"
    })
    // Continue upwards to 'source'
    .lineTo(2400, -700, {
        name: "source"
    })
    // Small arc downwards
    .arc(2250, -700, 150, 0, -Math.PI/2, true)

    //Line to 'follow'
    .lineTo(1350, -850, {
        name: "follow"
    })
    // Arc and rotate back to the beginning.
    .arc(1300, 50, 900, -Math.PI/2, -Math.PI, true, {
        rotate: Math.PI*2, 
        name: "end"
    });*/

    // We're done with the path, let's initate the plugin on our wrapper element
    $(".wrapper").scrollPath({
        drawPath: true, 
        wrapAround: true
    });

    // Add scrollTo on click on the navigation anchors
    $("nav #nav").find("a").each(function() {
        var target = $(this).attr("href").replace("#", "");
        console.log(target);
        $(this).click(function(e) {
            e.preventDefault();		
            // Include the jQuery easing plugin (http://gsgd.co.uk/sandbox/jquery/easing/)
            // for extra easing functions like the one below
            $.fn.scrollPath("scrollTo", target, 2000, 'easeInOutQuad');
        });
    });

    /* ===================================================================== */

    $(".share .tweet").click(function(e) {
        open(this.href, "", "width=550, height=450");
        e.preventDefault();
    });
    
    $(".settings .show-path").click(function(e) {
        e.preventDefault();
        $(".sp-canvas").toggle();
    }).toggle(function() {
        $(this).text("Hide Path");
    }, function() {
        $(this).text("Show Path");
    });
}


function highlight(element) {
    if(!element.hasClass("highlight")) {
        element.addClass("highlight");
        setTimeout(function() {
            element.removeClass("highlight");
        }, 2000);
    }
}
function ordinal(num) {
    return num + (
        (num % 10 == 1 && num % 100 != 11) ? 'st' :
        (num % 10 == 2 && num % 100 != 12) ? 'nd' :
        (num % 10 == 3 && num % 100 != 13) ? 'rd' : 'th'
        );
}
